# JKProgressHUD

Example project demonstrating how to use a new UIWindow to present content over existing application content (similar to a UIAlertView).

Blogpost on how this works: [Custom popups revisited](http://joris.kluivers.nl/blog/2012/03/02/custom-popups-revisited/)
